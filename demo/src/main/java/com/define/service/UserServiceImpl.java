package com.define.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.define.dao.UserDao;
import com.define.entity.LoginUser;

@Service
public class UserServiceImpl implements UserDetailsService{

	@Autowired
	private UserDao dao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LoginUser user = dao.findLoginUserByUsername(username);
		if(user==null) {
			throw new UsernameNotFoundException("没有这个账号");
		}
		return new User(user.getUsername(),user.getPassword(),getAuthority(user));
	}

	private Collection<? extends GrantedAuthority> getAuthority(LoginUser user) {
		String role = user.getRole();
		Set<GrantedAuthority> authority = new HashSet<GrantedAuthority>();
		String[] roles = role.split(",");
		for (String role1 : roles) {
			authority.add(new SimpleGrantedAuthority("ROLE_"+role1));
		}
		
		return authority;
	}

}
