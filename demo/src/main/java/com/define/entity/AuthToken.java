package com.define.entity;

public class AuthToken {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public AuthToken(String token) {
		super();
		this.token = token;
	}
	
}
