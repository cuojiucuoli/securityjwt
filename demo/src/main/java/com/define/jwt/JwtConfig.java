package com.define.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.buf.StringUtils;
import org.mockito.internal.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.define.util.TokenProvider;
@Component
public class JwtConfig extends OncePerRequestFilter{

	@Autowired
	TokenProvider tokenProvider;
	
	@Autowired
	UserDetailsService userDetailService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		//String header = request.getParameter("head");
		String token = request.getHeader("token");
		if (token!=null) {
			String username = tokenProvider.getUsernameFromToken(token);
			UserDetails userDetails = userDetailService.loadUserByUsername(username);
			if (userDetails != null) {
				Boolean isTrue = tokenProvider.validateToken(token, userDetails);
				if (isTrue == true) {
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
					;
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			} 
		}
		filterChain.doFilter(request, response);
	}

}
