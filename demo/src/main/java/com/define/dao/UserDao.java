package com.define.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.define.entity.LoginUser;

public interface UserDao extends MongoRepository<LoginUser,Integer>{

	LoginUser findLoginUserByUsername(String username);
}
