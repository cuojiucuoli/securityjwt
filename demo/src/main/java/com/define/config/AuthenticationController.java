package com.define.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.define.entity.AuthToken;
import com.define.entity.LoginUser;
import com.define.util.TokenProvider;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	TokenProvider jwtTokenUtil;
	//@Autowired
	//private 
	
	@RequestMapping(value="/getToken")
	public ResponseEntity register(String username,String password) {
		
		final Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		SecurityContextHolder.getContext().setAuthentication(authenticate);
		UserDetails userDetail =(UserDetails)authenticate.getDetails();
		final String token = jwtTokenUtil.generateToken(userDetail);
		return ResponseEntity.ok(new AuthToken(token));	
		
	}
}
